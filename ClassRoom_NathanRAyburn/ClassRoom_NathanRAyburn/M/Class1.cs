﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassRoom_NathanRayburn
{
    public class Student
    {


        #region private
        private string firstname;
        private string lastname;
       
        public Student()
        {

 
        }


        #endregion private
        public Student(string v1, string v2)
        {
            this.firstname = v1;
            this.lastname = v2;
        }
        #region constructor

        #endregion constructor
        #region ascenceur
        public string Firstname{

            set { firstname = value; }
            get { return firstname; }
        }

        public string Lastname {

            set { lastname = value.ToUpper(); }
            get { return lastname.ToUpper(); }

        }
        #endregion ascenceur

    }
    public class SchoolClass
    {

        #region private
        private List<Student> students = null;
        private Random a = null;
        private string name = null;
    
        #endregion private
        #region constructor
        public SchoolClass()
        {
            a = new Random();
            this.students = new List<Student>();
            
        }
        #endregion constructor

        #region methode
        public Student GetRandomStudent() {

            
            return Students[this.a.Next(0,students.Count-1)];
        }

        #endregion methode

        #region accessor
        public List<Student> Students
        {

            get { return this.students; }
            set { this.students = value; }

        }
        public string Name {

            get { return this.name; }

            set {if (value.Contains("-"))
                {
                    this.name = value;
                }

                else { throw new Exception(); }
                    }

        }
        #endregion accessor


    }
}
