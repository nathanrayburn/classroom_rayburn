﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsForm.View;
using Buisness.Model;
namespace WindowsForm
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            StudenLoader load = new StudenLoader();
            var schoolclasses = load.Schoolclasses;
       
            Application.Run(new FormClassRoom(schoolclasses));
        }
    }
}
