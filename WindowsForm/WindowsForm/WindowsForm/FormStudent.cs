﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buisness.Model;
namespace WindowsForm
{
    public partial class FormStudent : Form
    {
        private string firstname;
        private string lastname;
        private string link;
        private string id;
     

        public FormStudent()
        {
            

           
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Firstname"></param>
        /// <param name="Lastname"></param>
        /// <param name="Link"></param>
        public FormStudent(string Firstname, string Lastname,string Link, string ID)
        {
            InitializeComponent();
            this.firstname = Firstname;
            this.lastname = Lastname;
            this.link = Link;
            this.id = ID;

        }

       
        private void FormStudent_Load(object sender, EventArgs e)
        {
            txt_Name.Enabled = false;
            txt_Lastname.Enabled = false;
            txt_Lastname.Text = lastname;
            txt_Name.Text = firstname;
            pic_profile.ImageLocation = @link;
        }

        private void OK_student_Click(object sender, EventArgs e)
        {

            btn_file.Visible = true;
            btn_edit.Visible = false;
            btn_Save.Enabled = true;
           //this.DialogResult = DialogResult.OK;
        }

        private void Cancel_Student_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void GroupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {
           
        }

        private void Txt_Name_TextChanged(object sender, EventArgs e)
        {

        }

        private void FileSystemWatcher1_Changed(object sender, System.IO.FileSystemEventArgs e)
        {

        }

        private void Btn_file_Click(object sender, EventArgs e)
        {
            OpenFileDialog choofdlog = new OpenFileDialog();
              choofdlog.Filter = "All Files (*.*)|*.*";
              choofdlog.FilterIndex = 1;
              choofdlog.Multiselect = false;

              if (choofdlog.ShowDialog() == DialogResult.OK)
              {
                  string sFileName = choofdlog.FileName;
                //  string[] arrAllFiles = choofdlog.FileNames; //used when Multiselect = true           
              }
        }

        private void Btn_Save_Click(object sender, EventArgs e)
        {
            btn_file.Visible = false;
            btn_edit.Visible = true;
            btn_Save.Enabled = false;
        }
    }
}
