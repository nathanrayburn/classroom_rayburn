﻿namespace WindowsForm.View
{
    partial class FormClassRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.list_Students = new System.Windows.Forms.ListBox();
            this.combo_Classes = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // list_Students
            // 
            this.list_Students.FormattingEnabled = true;
            this.list_Students.Location = new System.Drawing.Point(40, 61);
            this.list_Students.Name = "list_Students";
            this.list_Students.Size = new System.Drawing.Size(192, 160);
            this.list_Students.Sorted = true;
            this.list_Students.TabIndex = 0;
            this.list_Students.SelectedIndexChanged += new System.EventHandler(this.ListBox1_SelectedIndexChanged);
            this.list_Students.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.List_Students_MouseDoubleClick);
            // 
            // combo_Classes
            // 
            this.combo_Classes.FormattingEnabled = true;
            this.combo_Classes.Location = new System.Drawing.Point(40, 23);
            this.combo_Classes.Name = "combo_Classes";
            this.combo_Classes.Size = new System.Drawing.Size(121, 21);
            this.combo_Classes.Sorted = true;
            this.combo_Classes.TabIndex = 2;
            this.combo_Classes.SelectedIndexChanged += new System.EventHandler(this.Combo_Classes_SelectedIndexChanged);
            // 
            // FormClassRoom
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 461);
            this.Controls.Add(this.combo_Classes);
            this.Controls.Add(this.list_Students);
            this.Name = "FormClassRoom";
            this.Text = "FormClassRoom";
            this.Load += new System.EventHandler(this.FormClassRoom_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox list_Students;
        private System.Windows.Forms.ComboBox combo_Classes;
    }
}