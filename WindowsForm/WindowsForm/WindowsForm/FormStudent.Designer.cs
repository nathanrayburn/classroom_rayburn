﻿namespace WindowsForm
{
    partial class FormStudent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStudent));
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_Cancel_Student = new System.Windows.Forms.Button();
            this.pic_profile = new System.Windows.Forms.PictureBox();
            this.gpr_Profile = new System.Windows.Forms.GroupBox();
            this.txt_Name = new System.Windows.Forms.TextBox();
            this.txt_Lastname = new System.Windows.Forms.TextBox();
            this.lbl_Lastname = new System.Windows.Forms.Label();
            this.lbl_Name = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.btn_file = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pic_profile)).BeginInit();
            this.gpr_Profile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(127, 207);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(75, 23);
            this.btn_edit.TabIndex = 0;
            this.btn_edit.Text = "edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.OK_student_Click);
            // 
            // btn_Cancel_Student
            // 
            this.btn_Cancel_Student.Location = new System.Drawing.Point(325, 207);
            this.btn_Cancel_Student.Name = "btn_Cancel_Student";
            this.btn_Cancel_Student.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel_Student.TabIndex = 1;
            this.btn_Cancel_Student.Text = "Cancel";
            this.btn_Cancel_Student.UseVisualStyleBackColor = true;
            this.btn_Cancel_Student.Click += new System.EventHandler(this.Cancel_Student_Click);
            // 
            // pic_profile
            // 
            this.pic_profile.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pic_profile.ErrorImage")));
            this.pic_profile.Image = ((System.Drawing.Image)(resources.GetObject("pic_profile.Image")));
            this.pic_profile.InitialImage = ((System.Drawing.Image)(resources.GetObject("pic_profile.InitialImage")));
            this.pic_profile.Location = new System.Drawing.Point(12, 38);
            this.pic_profile.Name = "pic_profile";
            this.pic_profile.Size = new System.Drawing.Size(107, 100);
            this.pic_profile.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_profile.TabIndex = 2;
            this.pic_profile.TabStop = false;
            this.pic_profile.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // gpr_Profile
            // 
            this.gpr_Profile.Controls.Add(this.txt_Name);
            this.gpr_Profile.Controls.Add(this.txt_Lastname);
            this.gpr_Profile.Controls.Add(this.lbl_Lastname);
            this.gpr_Profile.Controls.Add(this.lbl_Name);
            this.gpr_Profile.Location = new System.Drawing.Point(139, 38);
            this.gpr_Profile.Name = "gpr_Profile";
            this.gpr_Profile.Size = new System.Drawing.Size(288, 100);
            this.gpr_Profile.TabIndex = 3;
            this.gpr_Profile.TabStop = false;
            this.gpr_Profile.Text = "Personal Data";
            this.gpr_Profile.Enter += new System.EventHandler(this.GroupBox1_Enter);
            // 
            // txt_Name
            // 
            this.txt_Name.Location = new System.Drawing.Point(134, 15);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Size = new System.Drawing.Size(100, 20);
            this.txt_Name.TabIndex = 0;
            this.txt_Name.TextChanged += new System.EventHandler(this.Txt_Name_TextChanged);
            // 
            // txt_Lastname
            // 
            this.txt_Lastname.Location = new System.Drawing.Point(134, 41);
            this.txt_Lastname.Name = "txt_Lastname";
            this.txt_Lastname.Size = new System.Drawing.Size(100, 20);
            this.txt_Lastname.TabIndex = 1;
            // 
            // lbl_Lastname
            // 
            this.lbl_Lastname.AutoSize = true;
            this.lbl_Lastname.Location = new System.Drawing.Point(16, 46);
            this.lbl_Lastname.Name = "lbl_Lastname";
            this.lbl_Lastname.Size = new System.Drawing.Size(53, 13);
            this.lbl_Lastname.TabIndex = 2;
            this.lbl_Lastname.Text = "Lastname";
            // 
            // lbl_Name
            // 
            this.lbl_Name.AutoSize = true;
            this.lbl_Name.Location = new System.Drawing.Point(16, 22);
            this.lbl_Name.Name = "lbl_Name";
            this.lbl_Name.Size = new System.Drawing.Size(35, 13);
            this.lbl_Name.TabIndex = 3;
            this.lbl_Name.Text = "Name";
            // 
            // btn_Save
            // 
            this.btn_Save.Enabled = false;
            this.btn_Save.Location = new System.Drawing.Point(219, 207);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 23);
            this.btn_Save.TabIndex = 4;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.Btn_Save_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            this.fileSystemWatcher1.Changed += new System.IO.FileSystemEventHandler(this.FileSystemWatcher1_Changed);
            // 
            // btn_file
            // 
            this.btn_file.Location = new System.Drawing.Point(12, 158);
            this.btn_file.Name = "btn_file";
            this.btn_file.Size = new System.Drawing.Size(137, 23);
            this.btn_file.TabIndex = 5;
            this.btn_file.Text = "Choose  Picture File";
            this.btn_file.UseVisualStyleBackColor = true;
            this.btn_file.Visible = false;
            this.btn_file.Click += new System.EventHandler(this.Btn_file_Click);
            // 
            // FormStudent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(471, 450);
            this.Controls.Add(this.btn_file);
            this.Controls.Add(this.btn_Save);
            this.Controls.Add(this.gpr_Profile);
            this.Controls.Add(this.pic_profile);
            this.Controls.Add(this.btn_Cancel_Student);
            this.Controls.Add(this.btn_edit);
            this.Name = "FormStudent";
            this.Text = "FormStudent";
            this.Load += new System.EventHandler(this.FormStudent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pic_profile)).EndInit();
            this.gpr_Profile.ResumeLayout(false);
            this.gpr_Profile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_Cancel_Student;
        private System.Windows.Forms.PictureBox pic_profile;
        private System.Windows.Forms.GroupBox gpr_Profile;
        private System.Windows.Forms.TextBox txt_Name;
        private System.Windows.Forms.TextBox txt_Lastname;
        private System.Windows.Forms.Label lbl_Lastname;
        private System.Windows.Forms.Label lbl_Name;
        private System.Windows.Forms.Button btn_Save;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.Button btn_file;
    }
}