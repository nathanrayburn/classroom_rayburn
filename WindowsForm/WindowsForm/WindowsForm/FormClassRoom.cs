﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Buisness.Model;
namespace WindowsForm.View
{
    public partial class FormClassRoom : Form
    {


        private List<object> listClasses;

        /// <summary>
        /// 
        /// </summary>
        public FormClassRoom(List<object> schoolclasses)
        {
            
            InitializeComponent();
            this.listClasses = schoolclasses;
          
            
        }



        private void FormClassRoom_Load(object sender, EventArgs e)
        {
          
            foreach (SchoolClass classes in listClasses) {
                combo_Classes.Items.Add(classes.Name);
            }
      
            
           
  

        }

        private void ListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string fullname = null;
            object selected = null;


            foreach (SchoolClass classes in listClasses)
            {
                foreach (Student students in classes.Students)
                {
                    fullname = students.Firstname + " " + students.Lastname;
                    selected = list_Students.SelectedItem;
                    selected = selected.ToString();
                    if (fullname.Equals(selected))
                    {
                        string Firstname = students.Firstname;
                        string Lastname = students.Lastname;
                        string Link = students.Link;
                        string ID = students.ID;

                        FormStudent student = new FormStudent(Firstname, Lastname, Link, ID); //Create a Form Object
                        student.ShowDialog();
                    }
              
                }
            }


        }

        private void Button1_Click(object sender, EventArgs e)
        {

        }



        private void Combo_Classes_SelectedIndexChanged(object sender, EventArgs e)
        {
            list_Students.Items.Clear();
            foreach (SchoolClass classes in listClasses) {
                if (combo_Classes.Text == classes.Name) {

                    foreach (Student students in classes.Students)
                    {
                        list_Students.Items.Add(students.Firstname + " " + students.Lastname);
                    }

                }

            }
            
           

        }

        public void List_Students_MouseDoubleClick(object sender, MouseEventArgs e)
        {


        }
    }
}
