﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness
{
    public class Student
    {


        #region private
        private string firstname;
        private string lastname;
        private string link;
        private string id;

        public Student()
        {


        }

        public Student(string v1, string v2)
        {
            this.firstname = v1;
            this.lastname = v2;
        }


        #endregion private

        #region constructor
        public Student(string v1, string v2, string picture, string ID)
        {
            this.firstname = v1;
            this.lastname = v2;
            this.link = picture;
            this.id = ID;
        }
        #endregion constructor

        #region ascenceur
        public string Firstname
        {

            set { firstname = value; }
            get { return firstname; }
        }

        public string Lastname
        {

            set { lastname = value.ToUpper(); }
            get { return lastname.ToUpper(); }

        }

        public string Link
        {
            get { return link; }
            set { link = value; }
        }
        public string ID
        {
            get { return id; }
            set { id = value; }
        }
        #endregion ascenceur
        #region methodes

        public override string ToString()
        {
            return this.Lastname + " " + this.Lastname;
        }

        #endregion methodes

    }
}
