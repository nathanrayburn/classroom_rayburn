﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness
{
    public class StudenLoader
    {

        #region private



        #endregion private
        public StudenLoader()
        {



        }

        #region accessor



        public List<object> Schoolclasses

        {
            get
            {
                CSV readcsv = new CSV(); //Instancing the csv
                List<string> studentList = readcsv.ReadContent; //reading what content is in the csv file
                List<object> schoolclasses = null; //declaring a list class

                schoolclasses = new List<object>();

                foreach (string studentstring in studentList)
                {
                    /**
                     * initalizing and declaring
                     * 
                     * **/

                    List<string> studentInsert = studentstring.Split('\t').ToList(); //split de array of the csv content and turns into a list of string
                    Student students = new Student(studentInsert[2], studentInsert[3], studentInsert[1], studentInsert[0]); //creates an object of students and instances it

                    bool doubles = false; // this is a flag

                    if (!studentInsert[16].Contains("-")) //Skip the header line the csv file b checking if the class has a dash
                    {

                        continue;
                    }


                    // add student to his existing class if his class is already made
                    if (schoolclasses.Count == 0)
                    {
                        SchoolClass classes = new SchoolClass(); // add classes
                        classes.Name = studentInsert[16];
                        classes.Students.Add(students);
                        schoolclasses.Add(classes);

                    }
                    else
                    {
                        /**
                         * Going through the list of classes to check if the of the student that was just created already has an existing class
                         * 
                         * **/
                        foreach (SchoolClass checkClasses in schoolclasses)
                        {


                            if (checkClasses.Name == studentInsert[16]) //Creates a flag if the of the student that was just created already has an existing class
                            {
                                doubles = true; //the class already exist
                                break;
                            }

                        }

                        /**
                         * The class that the current student that is in doesn't exist
                         * **/
                        if (doubles == false)
                        {

                            SchoolClass classes = new SchoolClass(); // add classes
                            classes.Name = studentInsert[16]; //create the name of the class
                            classes.Students.Add(students); //add the current student to the class
                            schoolclasses.Add(classes); // add the class to the list of all classes


                        }
                        else
                        {
                            /**
                             * Adds the student if the class already exists
                             * 
                             **/

                            foreach (SchoolClass classes in schoolclasses)
                            {

                                if (studentInsert[16] == classes.Name) //Checks if the student has a class that already exists
                                {

                                    classes.Students.Add(students); //add the student to the class
                                    break; //stops the loop

                                }
                            }
                        }
                    }
                }

                return schoolclasses;

            }
        }
        #endregion accessor


    }
}
