﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Buisness
{
    public class CSV
    {


        #region private
        private string path = @"..\..\..\..\..\csv\data.csv";
        private string date;
        private string file;
        private List<string> content;

        #endregion private

        #region constructor


        #endregion constructor

        #region accessor
        public List<string> ReadContent
        {


            get
            {
                this.content = new List<string>();
                using (StreamReader rd = new StreamReader(path))
                {

                    string secondLine = File.ReadLines(path).ElementAtOrDefault(0);
                    while ((secondLine = rd.ReadLine()) != null)
                    {
                        this.content.Add(secondLine);

                    }


                    rd.Close(); //close the file

                }
                return this.content;
            }
        }
        #endregion accessor

    }
}
