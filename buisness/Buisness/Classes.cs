﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Buisness.Model
{
    public class Student
    {


        #region private
        private string firstname;
        private string lastname;
        private string link;
        private string id;

        public Student()
        {

 
        }

        public Student(string v1, string v2)
        {
            this.firstname = v1;
            this.lastname = v2;
        }


        #endregion private

        #region constructor
        public Student(string v1, string v2, string picture, string ID)
        {
            this.firstname = v1;
            this.lastname = v2;
            this.link = picture;
            this.id = ID;
        }
        #endregion constructor

        #region ascenceur
        public string Firstname{

            set { firstname = value; }
            get { return firstname; }
        }

        public string Lastname {

            set { lastname = value.ToUpper(); }
            get { return lastname.ToUpper(); }

        }

        public string Link
        {
            get { return link; } set { link = value; }
        }
        public string ID
        {
            get { return id; } set { id = value; }
        }
        #endregion ascenceur
        #region methodes

       public override string ToString()
        {
            return this.Lastname + " " + this.Lastname;
        }

        #endregion methodes

    }
    public class SchoolClass
    {

        #region private
        private List<Student> students = null;
        private Random a = null;
        private string name = null;
    
        #endregion private

        #region constructor
        public SchoolClass()
        {
            a = new Random();
            this.students = new List<Student>();
            
        }

        public Student GetRandomStudent()
        {


            return Students[this.a.Next(0, students.Count - 1)];
        }
        #endregion constructor

        #region accessor
        public List<Student> Students
        {

            get { return this.students; }
            set { this.students = value; }

        }
        public string Name {

            get { return this.name; }

            set {if (value.Contains("-"))
                {
                    this.name = value;
                }

                else { throw new Exception(); }
                    }

        }

        public static implicit operator List<object>(SchoolClass v)
        {
            throw new NotImplementedException();
        }
        #endregion accessor


    }
    public class StudenLoader
    {

        #region private



        #endregion private
        public StudenLoader()
        {



        }

        #region accessor



        public List<object> Schoolclasses

        {
            get
            {
                CSV readcsv = new CSV(); //Instancing the csv
                List<string> studentList = readcsv.ReadContent; //reading what content is in the csv file
                List<object> schoolclasses = null; //declaring a list class

                schoolclasses = new List<object>();

                foreach (string studentstring in studentList)
                {
                    /**
                     * initalizing and declaring
                     * 
                     * **/

                    List<string> studentInsert = studentstring.Split('\t').ToList(); //split de array of the csv content and turns into a list of string
                    Student students = new Student(studentInsert[2], studentInsert[3], studentInsert[1], studentInsert[0]); //creates an object of students and instances it

                    bool doubles = false; // this is a flag

                    if (!studentInsert[16].Contains("-")) //Skip the header line the csv file b checking if the class has a dash
                    {
                       
                        continue;
                    }


                    // add student to his existing class if his class is already made
                    if (schoolclasses.Count == 0)
                    {
                        SchoolClass classes = new SchoolClass(); // add classes
                        classes.Name = studentInsert[16];
                        classes.Students.Add(students);
                        schoolclasses.Add(classes);

                    }
                    else
                    {
                        /**
                         * Going through the list of classes to check if the of the student that was just created already has an existing class
                         * 
                         * **/
                        foreach (SchoolClass checkClasses in schoolclasses)
                        {


                            if (checkClasses.Name == studentInsert[16]) //Creates a flag if the of the student that was just created already has an existing class
                            {
                                doubles = true; //the class already exist
                                break;
                            }

                        }

                        /**
                         * The class that the current student that is in doesn't exist
                         * **/
                        if (doubles == false)
                        {

                            SchoolClass classes = new SchoolClass(); // add classes
                            classes.Name = studentInsert[16]; //create the name of the class
                            classes.Students.Add(students); //add the current student to the class
                            schoolclasses.Add(classes); // add the class to the list of all classes


                        }
                        else
                        {
                            /**
                             * Adds the student if the class already exists
                             * 
                             **/

                            foreach (SchoolClass classes in schoolclasses)
                            {

                                if (studentInsert[16] == classes.Name) //Checks if the student has a class that already exists
                                {

                                    classes.Students.Add(students); //add the student to the class
                                    break; //stops the loop

                                }
                            }
                        }
                    }
                }

                return schoolclasses;

            }
        }
#endregion accessor


    }

    public class CSV
    {


        #region private
        private string path = @"..\..\..\..\..\csv\data.csv";
        private string date;
        private string file;
        private List<string> content;

        #endregion private

        #region constructor


        #endregion constructor

        #region accessor
        public List<string> ReadContent
        {


            get
            {
                this.content = new List<string>();
                using (StreamReader rd = new StreamReader(path))
                {

                    string secondLine = File.ReadLines(path).ElementAtOrDefault(0);
                    while ((secondLine = rd.ReadLine()) != null)
                    {
                        this.content.Add(secondLine);

                    }


                    rd.Close(); //close the file

                }
                return this.content;
            }
        }
        #endregion accessor

    }
}
